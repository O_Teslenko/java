package lesson7.storage;

import lesson7.objects.Entity;
import lesson7.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 * Implementation of {@link lesson7.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link lesson7.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        //implement me according to interface by using extractResult method
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()){
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        Class<T> classType = (Class<T>) entity.getClass();
        Field nameDeleteField = classType.getDeclaredField("name");
        nameDeleteField.setAccessible(true);
        String sql = "DELETE FROM " + classType.getSimpleName() +
                " WHERE name = '" + nameDeleteField.get(entity) + "'";
        try(Statement statement = connection.createStatement()){
            return statement.execute(sql);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        Class<T> saveClass = (Class<T>) entity.getClass();

        String sql = null;
        String rows = "";
        String values = "";
        if (entity.isNew()) {
            Iterator<Map.Entry<String, Object>> entries = data.entrySet().iterator();
            while (entries.hasNext()){
                Map.Entry<String, Object> entry = entries.next();
                rows += entry.getKey();
                if (entry.getKey().equals("admin")){
                    int bool = entry.getValue().equals(true) ? 1 : 0;
                    values += "'" + bool + "'";
                }else values += "'" + entry.getValue() + "'";
                if (entries.hasNext()){
                    rows += ",";
                    values += ",";
                }
            }

            sql = "INSERT INTO " + saveClass.getSimpleName() + " (" + rows + ") VALUES (" + values + ")";
            try(Statement statement = connection.createStatement()) {
                statement.execute(sql);
            }
            //implement me
            //need to define right SQL query to create object
        } else {
            String str = "";
            Iterator<Map.Entry<String, Object>> entries = data.entrySet().iterator();
            while (entries.hasNext()){
                Map.Entry<String, Object> entry = entries.next();
                rows = entry.getKey();
                if (entry.getKey().equals("admin")){
                    int bool = entry.getValue().equals(true) ? 1 : 0;
                    values = "'" + bool + "'";
                }else values = "'" + entry.getValue() + "'";
                str += rows + "=" + values;
                if (entries.hasNext()){
                    str += ",";
                }
            }
            sql = "UPDATE " + saveClass.getSimpleName() + " SET " + str;
            try(Statement statement = connection.createStatement()) {
                statement.execute(sql);
            }
            //implement me
            //need to define right SQL query to update object
        }

        //implement me, need to save/update object and update it with new id if it's a creation
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        //implement me
        Map<String, Object> data = new HashMap<>();
        for (Field field : entity.getClass().getDeclaredFields()){
            field.setAccessible(true);
            if (field.isAnnotationPresent(Ignore.class)) continue;
            data.put(field.getName(), field.get(entity));
        }
        return data;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        //implement me
        List<T> result = new ArrayList<>();
        while (resultset.next()){
            Object typeClass = clazz.newInstance();
            for (Field field : typeClass.getClass().getDeclaredFields()){
                field.setAccessible(true);
                if (field.isAnnotationPresent(Ignore.class)) continue;
                field.set(typeClass, resultset.getObject(field.getName()));
            }
            result.add((T) typeClass);
        }
        return result;
    }
}
