package lesson2;

interface Driveable{
    void accelerate(int f, int t);
    void brake();
    void turn();
}
interface EnergyProvider{
    abstract void setFuel(int a);
    int getFuel();
}
interface ForceProvider{
    void setTime(int a);
    void setFuel(int a);
    double distance();
}
interface ForceAcceptor{
    void setVal(double d, int t);
    int getLeft();
    int getRight();
}

abstract class Vehicle implements Driveable{
    public abstract void accelerate(int f, int t);
    public abstract void brake();
    public abstract void turn();
}

class Car extends Vehicle{
    GasTank gasTank = new GasTank();
    DieselEngine dieselEngine = new DieselEngine();
    Wheels wheels = new Wheels();
    int fule;//количество топлива
    int time;//время в пути

    //установка времени и топлива
    public void accelerate(int f, int t) {
        fule = f;
        time = t;

        gasTank.setFuel(fule);//установка топлива в бак
        dieselEngine.setFuel(gasTank.getFuel());//установка топлива в двигтель
        dieselEngine.setTime(time);//установка времени в пути
    }

    //вывод расчетов после торможения
    public void brake() {
        System.out.println("За " + time + " чассов на автомобиле вы проехали " + dieselEngine.distance() + "км");
    }

    //вывод количества поворотов за время поездки
    public void turn() {
        wheels.setVal(dieselEngine.distance(), time);
        System.out.println("За время поездки на автомобиле вы повернули " + wheels.getLeft() + " раз налево и "
                + wheels.getRight() + " раз направо");
    }
}
class Boat extends Vehicle{
    GasTank gasTank = new GasTank();
    DieselEngine dieselEngine = new DieselEngine();
    Propeller propeller = new Propeller();
    int fule;//количество топлива
    int time;//время в пути

    //установка времени и топлива
    public void accelerate(int f, int t) {
        fule = f;
        time = t;

        gasTank.setFuel(fule);//установка топлива в бак
        dieselEngine.setFuel(gasTank.getFuel());//установка топлива в двигтель
        dieselEngine.setTime(time);//установка времени в пути
    }

    //вывод расчетов после торможения
    public void brake() {
        System.out.println("За " + time + " чассов на катере вы проехали " + dieselEngine.distance() + "км");
    }

    //вывод количества поворотов за время поездки
    public void turn() {
        propeller.setVal(dieselEngine.distance(), time);
        System.out.println("За время поездки на катере вы повернули " + propeller.getLeft() + " раз налево и "
                + propeller.getRight() + " раз направо");
    }
}
class SolarPowerCar extends Vehicle{
    SolarBattery solarBattery = new SolarBattery();
    ElectricEngine electricEngine = new ElectricEngine();
    Wheels wheels = new Wheels();
    int fule;//количество топлива
    int time;//время в пути

    //установка времени и топлива
    public void accelerate(int f, int t) {
        fule = f;
        time = t;

        solarBattery.setFuel(fule);//установка топлива в бак
        electricEngine.setFuel(solarBattery.getFuel());//установка топлива в двигтель
        electricEngine.setTime(time);//установка времени в пути
    }

    //вывод расчетов после торможения
    public void brake() {
        System.out.println("За " + time + " чассов на солнечном автомобиле вы проехали " + electricEngine.distance() + "км");
    }

    //вывод количества поворотов за время поездки
    public void turn() {
        wheels.setVal(electricEngine.distance(), time);
        System.out.println("За время поездки на солнечном автомобиле вы повернули " + wheels.getLeft() + " раз налево и "
                + wheels.getRight() + " раз направо");
    }
}

class GasTank implements EnergyProvider{
    private int fuel;
    public void setFuel(int a){
        fuel = a;
    }
    public int getFuel(){
        return fuel;
    }
}
class SolarBattery implements EnergyProvider{
    private int fuel;
    public void setFuel(int a){
        fuel = a;
    }
    public int getFuel(){
        return fuel;
    }
}

class DieselEngine implements ForceProvider{
    private int time;
    private final double FUEL_CONSUMPTION = .5;//расход двигателя
    private int fuel;

    public void setTime(int a){
        time = a;
    }
    public void setFuel(int a){
        fuel = a;
    }

    //вычисления дистанции
    public double distance(){
        return fuel / FUEL_CONSUMPTION * time;
    }
}
class ElectricEngine implements ForceProvider{
    private int time;
    private final double FUEL_CONSUMPTION = .2;//расход двигателя
    private int fuel;

    public void setTime(int a){
        time = a;
    }
    public void setFuel(int a){
        fuel = a;
    }

    //вычисления дистанции
    public double distance(){
        return fuel / FUEL_CONSUMPTION * time;
    }
}

class Wheels implements ForceAcceptor{
    private int val;

    public void setVal(double d, int t){
        val = (int) (d / t);
    }

    public int getLeft(){
        return (int) Math.floor(Math.random() * val);
    }
    public int getRight(){
        return (int) Math.floor(Math.random() * val);
    }

}
class Propeller implements ForceAcceptor{
    private int val;

    public void setVal(double d, int t){
        val = (int) (d / t);
    }

    public int getLeft(){
        return (int) Math.floor(Math.random() * val);
    }
    public int getRight(){
        return (int) Math.floor(Math.random() * val);
    }
}
public class Lesson_2 {
    public static void main (String[] args){
        Car car = new Car();
        car.accelerate(100 , 30);
        car.brake();
        car.turn();

        System.out.println();

        Boat boat = new Boat();
        boat.accelerate(20 , 3);
        boat.brake();
        boat.turn();

        System.out.println();

        SolarPowerCar solarPowerCar = new SolarPowerCar();
        solarPowerCar.accelerate(150 , 20);
        solarPowerCar.brake();
        solarPowerCar.turn();
    }
}
