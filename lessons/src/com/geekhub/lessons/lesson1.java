package com.geekhub.lessons;

//Факториал
public class lesson1 {
    static long factorial(int i){
        return  (i == 0 ? 1 : i * factorial(i - 1));
    }

    public static void main(String[] args){
        for(int i = 0; i < 16; i++) System.out.println(i+"! = "+factorial(i));
    }
}
