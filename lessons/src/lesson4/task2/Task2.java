package lesson4.task2;
import java.util.*;

public class Task2 {
    public static void main(String args[]){
        TaskControl taskControl = new TaskControl();
        Scanner scanner;
        int swKey;
        boolean flag = true;

        do{
            System.out.println("Органайзер.");
            System.out.println("111 - справка.");
            System.out.print("->");
            scanner = new Scanner(System.in);
            try{
                swKey = scanner.nextInt();
            }catch (InputMismatchException e){
                System.out.println("Only numbers");
                swKey = -1;
            }

            switch (swKey){
                case 0:
                    flag = false;
                    break;
                case 1:
                    String addCategory, addTask;
                    Calendar date = Calendar.getInstance();
                    boolean catFlag;
                    System.out.println("Создать задание.");

                    do{
                        catFlag = false;
                        System.out.println("Категория:");
                        System.out.println("1 - Создать.");
                        System.out.println("2 - Выбрать из существующих.");
                        System.out.print("->");
                        scanner = new Scanner(System.in);

                        try{
                            swKey = scanner.nextInt();
                        }catch (InputMismatchException e){
                            System.out.println("Only numbers");
                            swKey = -1;
                        }

                        switch (swKey){
                            case 1:
                                System.out.print("->");
                                scanner = new Scanner(System.in);
                                addCategory = scanner.nextLine();
                                break;
                            case 2:
                                Set<String> categoryes = new HashSet<String>(taskControl.getCategories());
                                int catCount = 1;
                                for (String cat : categoryes)System.out.println((catCount++) + " -> " +cat);
                                String[] s = categoryes.toArray(new String[categoryes.size()]);
                                System.out.print("->");
                                scanner = new Scanner(System.in);
                                try{
                                    swKey = scanner.nextInt();
                                    addCategory = s[swKey - 1];
                                }catch (InputMismatchException e){
                                    System.out.println("Only numbers");
                                    addCategory = null;
                                    catFlag = true;
                                }
                                break;
                            default:
                                addCategory = "No category";
                        }
                    }while (catFlag);

                    System.out.println("Создать задание:");
                    System.out.print("->");
                    scanner = new Scanner(System.in);
                    addTask = scanner.nextLine();

                    boolean dateFlag;
                    do{
                        dateFlag = false;
                        System.out.println("Установить дату(y/n)?");
                        scanner = new Scanner(System.in);

                        String s = scanner.nextLine();
                        if (s.equals("y")) {
                            System.out.println("dd.mm.yyyy.hh.mm");

                            scanner = new Scanner(System.in);
                            String dateFormat = scanner.nextLine();


                            String[] dateMas = dateFormat.split("\\.");

                            date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateMas[0]));
                            date.set(Calendar.MONTH, Integer.parseInt(dateMas[1]) - 1);
                            date.set(Calendar.YEAR, Integer.parseInt(dateMas[2]));
                            date.set(Calendar.HOUR, Integer.parseInt(dateMas[3]));
                            date.set(Calendar.MINUTE, Integer.parseInt(dateMas[4]));

                        }
                    }while (dateFlag);

                    taskControl.addTask(date.getTime(), new Task(addCategory, addTask));
                    break;
                case 2:
                    Map<String, List<Task>> map1 = new TreeMap<String, List<Task>>(taskControl.getTasksByCategories());
                    System.out.println("Список Заданий:");
                    for (String getKeys : map1.keySet()){
                        List<Task> tasks = map1.get(getKeys);
                        System.out.print(getKeys + " -> ");
                        for (Task t : tasks)System.out.print(t.getTask() + " ");
                        System.out.println();
                    }
                    break;
                case 3:
                    System.out.println("Список Заданий по категориям.");
                    System.out.println("Введите категорию:");
                    System.out.print("->");
                    scanner = new Scanner(System.in);
                    String category = scanner.nextLine();
                    List<Task> tasks = taskControl.getTasksByCategory(category);
                    System.out.print(category + " -> ");
                    for (Task task : tasks)System.out.print(task.getTask() + " ");
                    System.out.println();
                    break;
                case 4:
                    System.out.println("Список Заданий на сегодня:");
                    List<Task> taskList = new ArrayList<Task>(taskControl.getTasksForToday());
                    for (Task task : taskList) System.out.println(task.getCategory() + " -> " + task.getTask());
                    break;
                case 111:
                    System.out.println("0 - Выход.");
                    System.out.println("1 - Создать задание.");
                    System.out.println("2 - Список Заданий.");
                    System.out.println("3 - Список Заданий по категориям.");
                    System.out.println("4 - Список Заданий на сегодня.");
                    break;
            }
        }while(flag);
    }
}
