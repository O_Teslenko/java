package lesson4.task2;
import java.util.*;

public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeTask(Date date);
    public Collection<String> getCategories();
    //For next 3 methods tasks should be sorted by scheduled date
    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();
}
