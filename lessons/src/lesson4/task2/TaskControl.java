package lesson4.task2;
import java.util.*;

public class TaskControl implements TaskManager{
    Map<Date, Task> map = new TreeMap<Date, Task>();

    @Override
    public void addTask(Date date, Task task) {
        map.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> category = new HashSet<String>(){{
            add("first-of-oll");
            add("later");
            add("maybe");
        }};

        for (Task task : map.values()){
            category.add(task.getCategory());
        }
        return category;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> map1 = new HashMap<String, List<Task>>();
        for (Task forTask : map.values()){

            String category = forTask.getCategory();
            if (!map1.containsKey(category))map1.put(category, new ArrayList<Task>());

            List<Task> tasks = map1.get(category);
            tasks.add(forTask);
        }
        return map1;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        return getTasksByCategories().get(category);
    }

    @Override
    public List<Task> getTasksForToday() {
        Calendar naw = Calendar.getInstance();
        List<Task> dates = new ArrayList<Task>();

        for (Date date : map.keySet()){
            Calendar tc = Calendar.getInstance();
            tc.setTime(date);

            if (tc.get(Calendar.YEAR) == naw.get(Calendar.YEAR) &&
                    tc.get(Calendar.DAY_OF_YEAR) == naw.get(Calendar.DAY_OF_YEAR))
            dates.add(map.get(date));
        }
        return dates;
    }
}
