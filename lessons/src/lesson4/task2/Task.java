package lesson4.task2;
import java.util.*;

public class Task{
    private String task, category;

    Task(String c, String t){
        task = t;
        category = c;
    }

    public String getTask() {
        return task;
    }

    public String getCategory() {
        return category;
    }
}
