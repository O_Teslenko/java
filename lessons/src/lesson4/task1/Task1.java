package lesson4.task1;
import java.util.*;

public class Task1 {
    public static void main(String args[]){
        Operations operations = new Operations();
        Set<Integer> al1 = new HashSet<Integer>();
        Set<Integer> al2 = new HashSet<Integer>();

        for (int i = 0; i < 10; i++){
            al1.add(i);
            al2.add(i+5);
        }

        System.out.println("Множество А: " + al1);
        System.out.println("Множество B: " + al2);
        System.out.println("Реализация метода equals: " + operations.equals(al1, al2));
        System.out.println("Реализация метода union: " + operations.union(al1, al2));
        System.out.println("Реализация метода subtract: " + operations.subtract(al1, al2));
        System.out.println("Реализация метода intersect: " + operations.intersect(al1, al2));
        System.out.println("Реализация метода symmetricSubtract: " + operations.symmetricSubtract(al1, al2));
    }
}
