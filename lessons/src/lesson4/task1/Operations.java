package lesson4.task1;
import java.util.*;

public class Operations implements SetOperations{
    Set<Integer> al1 = new HashSet<Integer>();
    Set<Integer> al2 = new HashSet<Integer>();
    Set<Integer> al3 = new HashSet<Integer>();

    @Override
    public boolean equals(Set a, Set b) {
        al1.addAll(a);
        al2.addAll(b);
        return al1.equals(al2);
    }

    @Override
    public Set union(Set a, Set b) {
        al1.addAll(a);
        al2.addAll(b);
        al1.addAll(al2);
        return al1;
    }

    @Override
    public Set subtract(Set a, Set b) {
        al1.addAll(a);
        al2.addAll(b);
        al1.removeAll(al2);
        return al1;
    }

    @Override
    public Set intersect(Set a, Set b) {
        al1.addAll(a);
        al2.addAll(b);
        Iterator<Integer> is = al1.iterator();
        while (is.hasNext()){
            if (!al2.contains(is.next()))is.remove();
        }
        return al1;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        al1.addAll(a);
        al2.addAll(b);
        al3.addAll(a);
        Iterator<Integer> is = al1.iterator();
        Iterator<Integer> is2 = al2.iterator();
        while (is.hasNext()){
            if (al2.contains(is.next()))is.remove();
        }
        while (is2.hasNext()){
            if (al3.contains(is2.next()))is2.remove();
        }
        al1.addAll(al2);
        return al1;
    }
}
