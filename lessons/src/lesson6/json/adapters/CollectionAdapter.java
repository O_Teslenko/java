package lesson6.json.adapters;

import lesson6.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;
import java.util.Iterator;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        JSONArray m = new JSONArray();

        Iterator<Integer> oi = c.iterator();

        while (oi.hasNext()){
            m.put(JsonSerializer.serialize(oi.next()));
        }
        return m;
    }
}
