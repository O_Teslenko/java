package Translator;
import Translator.source.SourceLoader;
import Translator.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        System.out.println("Выберите способ загрузки контента:");
        System.out.println("'url' - c интернет ресурса.");
        System.out.println("'file' - c локального файла.");
        System.out.println("'exit' - выход.");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {
            String source = sourceLoader.loadSource(command);
            String translation = translator.translate(source);

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation.substring(81, translation.indexOf("</text>")));

            command = scanner.next();
        }
    }
}
