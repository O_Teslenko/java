package Translator.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        sourceProviders.add(new URLSourceProvider());
        sourceProviders.add(new FileSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException {
        SourceProvider backSourceProvider;
        String backStr;
        switch (pathToSource){
            case "url":
                backSourceProvider = sourceProviders.get(0);
                backStr = backSourceProvider.load("https://dl.dropboxusercontent.com/u/14434019/en.txt");
                break;
            case "file":
                backSourceProvider = sourceProviders.get(1);
                backStr = backSourceProvider.load("my/src/my/java/file1.txt");
                break;
            default:
                backStr = "again";
                break;
        }
        return backStr;
    }
}
