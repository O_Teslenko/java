package Translator.source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {
    private HttpURLConnection huc;
    @Override
    public boolean isAllowed(String pathToSource) {
        boolean flag = true;
        try {
            new URL(pathToSource);
        } catch (MalformedURLException e) {
            flag = false;
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return flag;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuilder result = new StringBuilder();
        if (isAllowed(pathToSource)){
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(pathToSource).openStream(), "UTF-8"))) {
                String str;
                while ((str = reader.readLine()) != null) {
                    result.append(str);
                }
            }
        }
        return result.toString();
    }
}
