package Translator.source;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File f = new File(pathToSource);
        if (f.exists())return true;
        else return false;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuffer outStr = new StringBuffer();
        if(isAllowed(pathToSource)){
            try (BufferedReader bis = new BufferedReader(new FileReader(pathToSource))){
                int c;
                while ((c = bis.read()) != -1) outStr.append((char) c);
            }
        }
        return outStr.toString();
    }
}
