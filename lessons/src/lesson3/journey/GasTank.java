package lesson3.journey;

public class GasTank implements EnergyProvider{
    private float maxTank;

    public void setMaxTan(float maxTank) {
        this.maxTank = maxTank;
    }

    public float provide(float fuel) throws ChargeFuelEx{
        if(fuel > maxTank)throw new ChargeFuelEx(0);
        return fuel;
    }
}
