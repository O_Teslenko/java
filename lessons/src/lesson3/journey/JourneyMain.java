package lesson3.journey;
import java.util.Scanner;

public class JourneyMain {
    static public void main(String args[]){
        float  maxTank, maxSpeed, on100km, maxEnergy;
        Drivable drivable;
        String vehicle;
        boolean flag, flagC = true;//флаги
        String first = "Здравствуйте какой вид транспорта выберете сегодня: ";
        String afterError = "Попробуйте ищо раз: ";
        Scanner scanner;

        System.out.println("'?' - справка.");
        do {
            scanner = new Scanner(System.in);
            flag = false;
            if (flagC) System.out.print(first);
            else System.out.print(afterError);
            vehicle = scanner.nextLine();

            if (vehicle.equals("1")) {
                maxTank = 120;
                on100km = 10;
                maxSpeed = 260;
                drivable = new Car(maxTank,on100km, maxSpeed);
                drivable.move();
            } else if (vehicle.equals("2")){
                maxEnergy = 200;
                on100km = 5;
                maxSpeed = 170;
                drivable = new SolarCar(maxEnergy,on100km, maxSpeed);
                drivable.move();
            } else if (vehicle.equals("3")){
                maxTank = 600;
                on100km = 40;
                maxSpeed = 130;
                drivable = new Boat(maxTank,on100km, maxSpeed);
                drivable.move();
            } else if (vehicle.equals("?")){
                flag = true;
                flagC = false;
                System.out.println("Справка:");
                System.out.println("'1' - Автомобиль.");
                System.out.println("'2' - Солнечный автомобиль.");
                System.out.println("'3' - Катер.");
                System.out.println();
            }else {
                flag = true;
                flagC = false;
                System.out.println("Ненайдено совпадений.");
                System.out.println();
            }
        }while (flag);
    }
}