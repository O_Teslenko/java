package lesson3.journey;

abstract class WheelVehicle extends Vehicle{
    Wheel wheel = new Wheel();

    WheelVehicle(float o100k, float ms) {
        super(o100k, ms);
    }
}
