package lesson3.journey;

public class ElectricEngine implements TorqueProvider{
    private float energy, on100km;

    public void setEnergy100km(float on100km) {
        this.on100km = on100km;
    }

    public float provideTorque(float energy) {
        return energy * on100km;
    }
}
