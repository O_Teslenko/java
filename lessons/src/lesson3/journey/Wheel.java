package lesson3.journey;

public class Wheel implements TorqueAcceptor{
    private float distance, maxSpeed, middleSpeed, consumption, speed;

    public void accept(float a, float ms){
        distance = a;
        maxSpeed = ms;
    }

    public float getSpeed() {
        return speed;
    }

    float getSpeed(float gs) throws SpeedLimitEx{
        if (gs > maxSpeed)throw new SpeedLimitEx(0);
        speed = gs;
        middleSpeed = maxSpeed / 2;
        consumption =  speed - middleSpeed;
        if (consumption < 0) consumption *= (-1);

        distance -= consumption;
        return distance;
    }
}
