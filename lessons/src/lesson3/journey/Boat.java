package lesson3.journey;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Boat extends FloatingVehicle{
    private float provideFuel, provideTorque, distance, ollWay, ollFuel, addFuel, newSpeed;
    private boolean flag;

    Propeller propeller = new Propeller();

    public Boat(float mt, float o100k, float ms) {
        super(o100k,ms);
        maxTank = mt;
    }

    public void accelerate(float fuel, float speed) {
        gasTank.setMaxTan(maxTank);//обьем бака
        try {
            provideFuel = gasTank.provide(fuel);//топлива в баке
        } catch (ChargeFuelEx e) {
            addFuel = 0;
            try {
                provideFuel = gasTank.provide(e.getVal());
            } catch (ChargeFuelEx chargeFuelEx) {
                System.out.println("Чтото пошло нетак.");
            }
            System.out.println(e);
        }
        dieselEngine.setFuel100km(on100km);//расход топлива
        provideTorque = dieselEngine.provideTorque(provideFuel);//предполагаемое растояние
        propeller.accept(provideTorque, maxSpeed);
        try {
            distance = propeller.getSpeed(speed);//пройденое растояние
        } catch (SpeedLimitEx e) {
            try {
                distance = propeller.getSpeed(speed);//пройденое растояние
            } catch (SpeedLimitEx ex) {
                System.out.println(ex);
            }
        }
    }

    public void move(){
        Scanner scanner;
        String choice;

        System.out.println("Вы выбрали катер.");
        System.out.println("Максемальналя скорость: " + maxSpeed + " км/час");
        System.out.println("Обьем бака: " + maxTank + " литров");

        do{
            scanner = new Scanner(System.in);
            if (flag){
                ollWay += distance;
                ollFuel += addFuel;
                System.out.println("Вы проплыли " + distance + "км со скоростью " + propeller.getSpeed() + "км/час");
                System.out.print("Желает продолжить путь? (y/n): ");
                choice = scanner.nextLine();
            }else choice = "y";
            flag = true;

            if (choice.equals("y")){
                try {
                    System.out.print("Заправить: ");
                    addFuel = scanner.nextFloat();
                    System.out.print("Выбрать скарость: ");
                    newSpeed = scanner.nextFloat();
                    accelerate(addFuel, newSpeed);
                }catch (InputMismatchException e){
                    addFuel = 0;
                    newSpeed = 0;
                    distance = 0;
                    try {
                        propeller.getSpeed(newSpeed);
                    } catch (SpeedLimitEx speedLimitEx) {
                        speedLimitEx.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    System.out.println("Ввести можна только цифры!");
                    flag = true;
                }
            }else{
                flag = false;
                brake(ollFuel, ollWay);
            }
        }while(flag);
    }

    public void brake(float f, float d) {
        System.out.println("Вы проплыли " + d + " км истратив " + f +" литров топлива.");
    }
}
