package lesson3.journey;

public interface Drivable {
    void accelerate(float a, float s);
    void brake(float f, float d);
    void move();
}
