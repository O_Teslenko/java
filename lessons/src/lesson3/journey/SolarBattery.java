package lesson3.journey;

public class SolarBattery implements EnergyProvider {
    private float maxEnergy;

    public void maxEnergy(float maxEnergy) {
        this.maxEnergy = maxEnergy;
    }

    public float provide(float energy) throws ChargeFuelEx{
        if(energy > maxEnergy)throw new ChargeFuelEx(maxEnergy);
        return energy;
    }
}
