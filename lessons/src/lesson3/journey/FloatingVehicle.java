package lesson3.journey;

abstract class FloatingVehicle extends Vehicle {

    public FloatingVehicle(float o100k, float ms) {
        super(o100k, ms);
    }
}
