package lesson3.journey;

public interface EnergyProvider {
    float provide(float p) throws Exception;
}
