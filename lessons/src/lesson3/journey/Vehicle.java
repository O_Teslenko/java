package lesson3.journey;

public abstract class Vehicle implements Drivable{
    public float maxTank, fuel, maxSpeed, on100km;

    GasTank gasTank = new GasTank();
    DieselEngine dieselEngine = new DieselEngine();

    Vehicle(float o100k, float ms){
        on100km = o100k;
        maxSpeed = ms;
    }

    public abstract void accelerate(float fuel, float speed);
    public abstract void brake(float f, float d);
}
