package lesson3.journey;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SolarCar extends WheelVehicle{
    private float provideEnergy, provideTorque, distance, energy, maxEnergy, ollWay, ollEnergy, addEnergy, newSpeed;
    private boolean flag;

    ElectricEngine electricEngine = new ElectricEngine();
    SolarBattery solarBattery = new SolarBattery();

    public SolarCar(float ms, float o100k ,float me) {
        super(o100k,ms);
        maxEnergy = me;
    }

    public void accelerate(float fuel, float speed) {
        solarBattery.maxEnergy(maxEnergy);//обьем батареи
        try {
            provideEnergy = solarBattery.provide(fuel);//зарад в батареи
        } catch (ChargeFuelEx e) {
            addEnergy = 0;
            try {
                provideEnergy = gasTank.provide(e.getVal());
            } catch (ChargeFuelEx chargeFuelEx) {
                //System.out.println("Чтото пошло нетак.");
            }
            System.out.println(e);
        }
        electricEngine.setEnergy100km(on100km);//расход топлива
        provideTorque = electricEngine.provideTorque(provideEnergy);//предполагаемое растояние
        wheel.accept(provideTorque, maxSpeed);
        try {
            distance = wheel.getSpeed(speed);//пройденое растояние
        } catch (SpeedLimitEx e) {
            System.out.println(e);
        }
    }

    public void move(){
        Scanner scanner;
        String choice;

        System.out.println("Вы выбрали солнечный автомобиль.");
        System.out.println("Максемальналя скорость: " + maxSpeed + " км/час");
        System.out.println("Заряд батареи: " + maxEnergy);

        do{
            scanner = new Scanner(System.in);
            if (flag){
                ollWay += distance;
                ollEnergy += addEnergy;
                System.out.println("Вы проехали " + distance + "км со скоростью " + wheel.getSpeed() + "км/час");
                System.out.print("Желает продолжить путь? (y/n): ");
                choice = scanner.nextLine();
            }else choice = "y";
            flag = true;

            if (choice.equals("y")){
                try {
                    System.out.print("Заправить: ");
                    addEnergy = scanner.nextFloat();
                    System.out.print("Выбрать скарость: ");
                    newSpeed = scanner.nextFloat();
                    accelerate(addEnergy, newSpeed);
                }catch (InputMismatchException e){
                    addEnergy = 0;
                    newSpeed = 0;
                    distance = 0;
                    try {
                        wheel.getSpeed(newSpeed);
                    } catch (SpeedLimitEx speedLimitEx) {
                        speedLimitEx.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    System.out.println("Ввести можна только цифры!");
                    flag = true;
                }
            }else{
                flag = false;
                brake(ollEnergy, ollWay);
            }
        }while(flag);
    }

    public void brake(float f, float d) {
        System.out.println("Вы проехали " + d + " км истратив " + f +" энергии.");
    }
}
