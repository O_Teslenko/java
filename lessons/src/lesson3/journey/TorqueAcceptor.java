package lesson3.journey;

public interface TorqueAcceptor {
    void accept(float a, float ms);
}
