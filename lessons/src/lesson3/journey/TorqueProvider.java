package lesson3.journey;

public interface TorqueProvider {
    float provideTorque(float pt);
}
