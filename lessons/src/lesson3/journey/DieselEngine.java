package lesson3.journey;

public class DieselEngine implements TorqueProvider{
    private float fuel, on100km;

    public void setFuel100km(float on100km) {
        this.on100km = on100km;
    }

    public float provideTorque(float fuel) {
        return fuel * on100km;
    }
}
