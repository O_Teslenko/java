package lesson3.journey;

public class SpeedLimitEx extends Exception{
    private float maxSpeed;

    SpeedLimitEx(float ms){
        maxSpeed = ms;
    }

    public String toString(){
        return "Скорость превышена!";
    }

    public float getVal(){
        return maxSpeed;
    }
}
