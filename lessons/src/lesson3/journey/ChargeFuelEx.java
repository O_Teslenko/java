package lesson3.journey;

public class ChargeFuelEx extends Exception{
   private float chargeFuel;

    public String toString(){
        return "Переполнение бака.";
    }

    ChargeFuelEx(float cf){
        chargeFuel = cf;
    }

    public float getVal(){
        return chargeFuel;
    }
}
