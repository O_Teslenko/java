package lesson3.TimeMillis;

public class StringBufferL3 extends Strings {
    StringBuffer str1 = new StringBuffer("Hello");
    StringBuffer str2 = new StringBuffer("World");

    public void timeOut(){
        long time = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++){
            StringBuffer str = str1.append(str2);
        }

        long timeOut = System.currentTimeMillis();
        System.out.println("Класс StringBuffer сделал 10000 итераций за " + (timeOut - time));
    }
}
