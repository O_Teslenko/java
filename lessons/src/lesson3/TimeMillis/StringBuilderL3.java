package lesson3.TimeMillis;

public class StringBuilderL3 extends Strings {
    StringBuilder str1 = new StringBuilder("Hello");
    StringBuilder str2 = new StringBuilder("World");

    public void timeOut(){
        long time = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++){
            StringBuilder str = str1.append(str2);
        }

        long timeOut = System.currentTimeMillis();
        System.out.println("Класс StringBuilder сделал 10000 итераций за " + (timeOut - time));
    }
}
