package lesson3.Comparable;

class Users implements Comparable {

    public String login;
    public int password;

    Users(String l, int p){
        login = l;
        password = p;
    }

    public int compareTo(Object o) {
        Users tmp = (Users)o;

        int res = login.compareTo(tmp.login);
        if (res != 0)return res;

        res = password - tmp.password;
        if (res != 0) return (int) res / Math.abs(res);
        return 0;
    }
}
