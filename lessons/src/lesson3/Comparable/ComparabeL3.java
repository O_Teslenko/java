package lesson3.Comparable;
import java.util.TreeSet;

public class ComparabeL3 {
    static public void main(String args[]){
        TreeSet<Users> ts = new TreeSet<Users>();
        ts.add(new Users("Mike", 45387));
        ts.add(new Users("Emma", 32465));
        ts.add(new Users("Emma", 12345));
        ts.add(new Users("Antonio", 87900));
        ts.add(new Users("Curt", 54367));
        ts.add(new Users("Kain", 11111));

        for (Users t : ts)System.out.println("login: " + t.login + " password:" + t.password);
    }
}
