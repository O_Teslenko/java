package lesson8;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Represents worker that downloads image from URL to specified folder.<br/>
 * Name of the image will be constructed based on URL. Names for the same URL will be the same.
 */
public class ImageTask implements Runnable {
    private URL url;
    private String folder;

    public ImageTask(URL url, String folder) {
        this.url = url;
        this.folder = folder;
    }

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    @Override
    public void run() {
        try {
            URLConnection imgUrl = url.openConnection();
            int len = imgUrl.getContentLength();
            byte imgData[] = new byte[len];
            InputStream is = imgUrl.getInputStream();
            int c = 0;
            do{
                c += is.read(imgData, c, len - c);
            } while(c < len);
            String saveString = folder + buildFileName(url);
            OutputStream os = new FileOutputStream(saveString);
            os.write(imgData);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //implement me
    }

    //converts URL to unique file name
    private String buildFileName(URL url) {
        return url.toString().replaceAll("[^a-zA-Z0-9-_\\.]", "_");
    }
}
