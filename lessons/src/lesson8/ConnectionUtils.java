package lesson8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        int c;
        URLConnection urlC = url.openConnection();
        InputStream input = urlC.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while ((c = input.read()) != -1){
            baos.write(c);
        }
        baos.flush();
        return baos.toByteArray();
        //implement me
    }
}
